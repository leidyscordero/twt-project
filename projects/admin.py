from django.contrib import admin
from .models import Proyecto, Avance, Liquidacion, Pago, Acceso
from ratings.models import Calificacion

class Calificaciones(admin.TabularInline):
   model = Calificacion
   raw_id_fields = ('trabajador','proveedor')
   verbose_name_plural = 'Calificacion'
   suit_form_inlines_hide_original = True
   extra = 1

#admin liquidacion
class Liquidaciones(admin.TabularInline):
    model = Liquidacion
    raw_id_fields = ('proyecto','trabajador')
    extra = 1
    verbose_name_plural = 'Contratos'
    suit_form_inlines_hide_original = True
   

#admin acceso
class Pagos(admin.TabularInline):
   model = Pago
   raw_id_fields = ('proyecto','proveedor')
   verbose_name_plural = 'Pagar'
   suit_form_inlines_hide_original = True
   extra = 1


# # admin proyectos
class Proyectos(admin.ModelAdmin):
    list_filter = ('estado',)
    search_fields = ('nombre',)
    list_display = ['id','nombre', 'fecha_inicio', 'fecha_final', 'direccion', 'estado','fecha_creacion']
    inlines = [
        Liquidaciones, Pagos
    ]
    class Meta:
        model = Proyecto

# # admin progreso
class Avances(admin.ModelAdmin):
    # list_filter = ('status',)
    search_fields = ('name','proyecto',)
    list_display = ['id','proyecto', 'avance']

    class Meta:
        model = Avance


class Liquidacion_contratos(admin.ModelAdmin):

    def calificacion(self, obj):
        califi =  Calificacion.objects.get(id=obj.id)
        return califi.promedio

    # list_filter = ('status',)
    search_fields = ('name','proyecto',)
    list_display = ['proyecto', 'fase','trabajador','valor_dia','dia_inicio','dia_final','salario','calificacion']
    inlines = [
        Calificaciones
    ]

    class Meta:
        model = Liquidacion


class Accesos(admin.ModelAdmin):

    # list_filter = ('status',)
    search_fields = ('name','proyecto',)
    list_display = ['proyecto', 'trabajador','hora_entrada','fecha_creacion','hora_salida']

    class Meta:
        model = Acceso


admin.site.register(Proyecto, Proyectos)
admin.site.register(Avance, Avances)
admin.site.register(Liquidacion, Liquidacion_contratos)
admin.site.register(Acceso, Accesos)
# admin.site.register(Payments, Payment)

