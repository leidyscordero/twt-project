from django.db import models
from django.utils import timezone
from peoples.models import Trabajador, Proveedor, Fase

from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save

# Create your models here.
class Proyecto(models.Model):
    nombre = models.CharField(max_length=30, null=False, blank=False)
    fecha_inicio = models.DateTimeField(null=False, blank=False)
    fecha_final = models.DateTimeField(null=False, blank=False)
    direccion = models.CharField(max_length=30, null=False)
    latitud = models.CharField(max_length=10, null=True, blank=True)
    longitd = models.CharField(max_length=10, null=True, blank=True)
    planos = models.ImageField(upload_to='file/blueprint', max_length=200, null=True, blank=True)
    trabajador = models.ManyToManyField(Trabajador, through='Liquidacion', related_name='project_workings',
                                        verbose_name='trabajadores', blank=False,
                                        through_fields=('proyecto', 'trabajador'))
    proveedor = models.ManyToManyField(Proveedor, through='pago', related_name='project_providers',
                                        verbose_name='provider', blank=False,
                                        through_fields=('proyecto', 'proveedor'))
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"
        default_permissions = ()
        permissions = (
            ("add_project", "Puede guardar proyectos"),
            ("view_project", "Puede ver proyectos"),
            ("change_project", "Puede actualizar proyectos"),
            ("delete_project", "Puede eliminar proyectos"),
        )

    def __str__(self):
        return str(self.nombre)

# Create your models here.
class Avance(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=True, blank=True) 
    avance = models.ImageField(upload_to='progress', max_length=200, null=True, blank=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)


    class Meta:
        verbose_name = "Avance"
        verbose_name_plural = "Avances"
        default_permissions = ()
        permissions = (
            ("add_progress", "Puede guardar avances"),
            ("view_progress", "Puede ver avances"),
            ("change_progress", "Puede actualizar avances"),
            ("delete_progress", "Puede eliminar avances"),
        )

    def __str__(self):
        return str(self.proyecto)

class Liquidacion(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=True, blank=True)
    trabajador = models.ForeignKey(Trabajador, on_delete=models.PROTECT, null=True, blank=True)
    fase = models.ForeignKey(Fase, on_delete=models.PROTECT, null=True, blank=True)
    valor_dia = models.FloatField(null=False)
    dia_inicio = models.DateField(null=True, blank=True)
    dia_final = models.DateField(null=True, blank=True)
    salario = models.FloatField(null=True, blank=True)
    # salud = models.FloatField(null=True, blank=True)
    # pension = models.FloatField(null=True, blank=True)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)


    class Meta:
        verbose_name = "Liquidación salarial"
        verbose_name_plural = "Liquidaciones salariales"
        default_permissions = ()
        permissions = (
            ("add_liquidations", "Puede guardar liquidaciones salariales"),
            ("view_liquidations", "Puede ver liquidaciones salariales"),
            ("change_liquidations", "Puede actualizar liquidaciones salariales"),
            ("delete_liquidations", "Puede eliminar liquidaciones salariales"),
        )

    def __str__(self):
        return str(str(self.trabajador.nombre) +str(" ") +str(self.trabajador.apellidos) + str(" -- ") + str(self.proyecto))

@receiver(pre_save, sender=Liquidacion)
def save_Liquidacion(sender, instance, **kwargs):
    if instance.valor_dia is None and instance.salario is not None:
        instance.valor_dia = (instance.salario / 30)
    elif instance.valor_dia is not None and instance.salario is None:
        instance.salario = instance.valor_dia * 30
    else:
        instance.salario = instance.valor_dia * 30

    # base = (instance.salario * 40) / 100)
    # salud = (base * 12.5 / 100 )
    # pension = (base * 16 )/100
    # instance.salud = salud
    # instance.pension = pension

class Pago(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=True, blank=True)
    proveedor = models.ForeignKey(Proveedor, on_delete=models.PROTECT, null=True, blank=True)
    valor = models.FloatField(null=False, blank=False)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Pago"
        verbose_name_plural = "Pagos"
        default_permissions = ()
        permissions = (
            ("add_payments", "Puede guardar pagos"),
            ("view_payments", "Puede ver pagos"),
            ("change_payments", "Puede actualizar pagos"),
            ("delete_payments", "Puede eliminar pagos"),
        )

    def __str__(self):
        return str(self.proveedor)


#Acceso
class Acceso(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=True, blank=True) 
    trabajador = models.ForeignKey(Trabajador, on_delete=models.PROTECT, null=True, blank=True)
    hora_entrada = models.DateTimeField(default=timezone.now)
    hora_salida = models.DateTimeField(null=True, blank=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Acceso"
        verbose_name_plural = "Accesos"
        default_permissions = ()
        permissions = (
            ("add_access", "Puede guardar accesos"),
            ("view_access", "Puede ver accesos"),
            ("change_access", "Puede actualizar accesos"),
            ("delete_access", "Puede eliminar accesos"),
        )

    def __str__(self):
        return str(str(self.trabajador) + str(" ") + str(self.proyecto))

