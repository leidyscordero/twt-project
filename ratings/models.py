from django.db import models
from django.utils import timezone
from peoples.models import Trabajador, Proveedor
# from projects.models import Liquidacion

from django.core.validators import MaxValueValidator, MinValueValidator

from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from projects.models import Liquidacion


# Create your models here.
# calificacion
class Calificacion(models.Model):
    liquidacion = models.ForeignKey(Liquidacion, on_delete=models.PROTECT, null=False, blank=False)
    trabajador = models.ForeignKey(Trabajador, on_delete=models.PROTECT, null=True, blank=True)
    proveedor = models.ForeignKey(Proveedor, on_delete=models.PROTECT, null=True, blank=True)
    puntualidad = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    responsabilidad =models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    eficiencia = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    eficacia = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    normas_seguridad = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    trabajo_equipo = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    promedio = models.PositiveIntegerField(default=10, validators=[MinValueValidator(1), MaxValueValidator(10)])
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Calificación"
        verbose_name_plural = "Calificaciones"
        default_permissions = ()
        permissions = (
            ("add_ratings", "Puede guardar calificaciones"),
            ("view_ratings", "Puede ver calificaciones"),
            ("change_ratings", "Puede actualizar calificaciones"),
            ("delete_ratings", "Puede eliminar calificaciones"),
        )

    def __str__(self):
        return str(self.id)


#disparador antes de guardar
@receiver(pre_save, sender=Calificacion)
def save_calificacion(sender, instance, **kwargs):
    promedio = (instance.puntualidad + instance.responsabilidad +  instance.eficacia + instance.eficiencia + instance.normas_seguridad + instance.trabajo_equipo)/6
    instance.promedio = promedio
