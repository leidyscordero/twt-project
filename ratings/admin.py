from django.contrib import admin
from .models import Calificacion

# Register your models here.

# admin proyectos
class Calificaciones(admin.ModelAdmin):
    list_filter = ('id',)
    search_fields = ('id',)
    list_display = ['id','liquidacion', 'puntualidad', 'responsabilidad', 'eficiencia','eficacia','normas_seguridad','trabajo_equipo']

    class Meta:
        model = Calificacion

admin.site.register(Calificacion, Calificaciones)