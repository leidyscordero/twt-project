from django.contrib import admin
from .models import Personas, Trabajador, Proveedor
from ratings.models import Calificacion

# admin personas
# class Peoples(admin.ModelAdmin):
#     list_filter = ('status',)
#     search_fields = ('name','business_name', 'type_document','name','nid','last_name',)
#     list_display = ['id','business_name', 'type_document', 'nid', 'name', 'last_name', 'status','created_at']

#     class Meta:
#         model = People

class Trabajadores(admin.ModelAdmin):

    def calificacion(self, obj):
        califi =  Calificacion.objects.filter(trabajador=obj.id)
        count = 0
        promedio = 0 
        for key in califi:
            promedio = promedio + key.promedio 
            count +=1

        if count == 0:
            return str("Sin Calificación")  
        else: return int(promedio / count)

    list_filter = ('estado','profesion')
    search_fields = ('nombre','Numero_Tarjeta','apellidos', 'tipo_documento','nombre','nid','razon_social',)
    list_display = ['id','razon_social', 'tipo_documento', 'numero_tarjeta', 'nid', 'nombre', 'apellidos',  'telefono',  'arl','eps','profesion','estado','fecha_creacion','calificacion']

    class Meta:
        model = Trabajador


class Proveedores(admin.ModelAdmin):
    search_fields = ('nombre','apellidos', 'tipo_documento','nombre','nid','razon_social',)
    list_display = ['id','razon_social', 'tipo_documento', 'nid', 'nombre', 'apellidos',  'telefono', 'estado','fecha_creacion']

    class Meta:
        model = Proveedor


admin.site.register(Trabajador, Trabajadores)
admin.site.register(Proveedor, Proveedores)

# admin.site.register(People, Peoples)