from django.db import models
from django.utils import timezone
from core.models import *

# Create your models here.
# persona
class Personas(models.Model):
    business_name = (
        ('PERSONA NATURAL','PERSONAL NATURAL'),
        ('PERSONA JURIDICA','PERSONA JURIDICA')
    )

    profesion = models.ForeignKey(Profesion, on_delete=models.PROTECT, blank=False, null=False)
    razon_social = models.CharField(max_length=100, null=False, choices=business_name)
    tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.PROTECT, null=False, blank=False)
    nid = models.CharField(max_length=50, null=False, blank=False)
    nombre = models.CharField(max_length=100, null=True)
    apellidos = models.CharField(max_length=100, null=True, blank=True)
    telefono = models.CharField(max_length=15, null=False)
    direccion = models.CharField(max_length=30, null=False)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"
        default_permissions = ()
        permissions = (
            ("add_peoples", "Puede guardar personas"),
            ("view_peoples", "Puede ver personas"),
            ("change_peoples", "Puede actualizar personas"),
            ("delete_peoples", "Puede eliminar personas"),
        )
    def __str__(self):
        return str(self.nombre)


class Trabajador(Personas):
    numero_tarjeta = models.CharField(max_length=50, null=False, blank=False)
    arl = models.ForeignKey(Arl, on_delete=models.PROTECT, null=True, blank=True)
    eps = models.ForeignKey(Eps, on_delete=models.PROTECT, null=True, blank= True)

    class Meta:
        verbose_name = "Trabajador"
        verbose_name_plural = "Tabajadores"
        default_permissions = ()
        permissions = (
            ("add_working", "Puede guardar trabajadores"),
            ("view_working", "Puede ver trabajadores"),
            ("change_working", "Puede actualizar trabajadores"),
            ("delete_working", "Puede eliminar trabajadores"),
        )

    def __str__(self):
        return str(self.nombre)


class Proveedor(Personas):

    class Meta:
        verbose_name = "Proveedor"
        verbose_name_plural = "Proveedores"
        default_permissions = ()
        permissions = (
            ("add_provider", "Puede guardar proveedores"),
            ("view_provider", "Puede ver proveedores"),
            ("change_provider", "Puede actualizar proveedores"),
            ("delete_provider", "Puede eliminar proveedores"),
        )

    def __str__(self):
        return str(self.nombre)

