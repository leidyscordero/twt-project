from suit.apps import DjangoSuitConfig
from suit.menu import ParentItem, ChildItem

class SuitConfig(DjangoSuitConfig):
    #layout = 'horizontal'
    name = 'suit'
    verbose_name = 'Django Suit'
    # Menu and header layout - horizontal or vertical
    layout = 'horizontal'

    # Set default list per page
    list_per_page = 20

    # Show changelist top actions only if any row is selected
    toggle_changelist_top_actions = True

    # Define menu
    #: :type: list of suit.menu.ParentItem
    menu = (
        ParentItem('Trabajadores & Proveedores', children=[
            ChildItem(model='peoples.trabajador'),
            ChildItem(model='peoples.proveedor'),
            #ChildItem(model='prestamos.detalleprestamo'),
        ]),
        ParentItem('Proyectos', children=[
            ChildItem(model='projects.proyecto'),
            ChildItem(model='projects.pago'),
            ChildItem(model='projects.avance'),
            ChildItem(model='projects.liquidacion'),
        ]),
        ParentItem('Calificaciones', children=[
            ChildItem(model='ratings.calificacion'),
        ]),
        # ParentItem('Incidentes', children=[
        #     ChildItem(model='prestamos.incidente'),
        #     # ChildItem(model='prestamos.detalleincidente'),
        # ]),
        # ParentItem('Reportes', children=[
        #     ChildItem('Reporte ', url='/admin/Reporte/'),
        # ]),
        ParentItem('Configuracion', children=[
            ChildItem(model='core.tipodocumento'),
            ChildItem(model='core.arl'),
            ChildItem(model='core.eps'),
            ChildItem(model='core.fase'),
            ChildItem(model='core.profesion'),
        ]),
        ParentItem('Gestion de usuarios', children=[
            ChildItem(model='auth.user'),
            ChildItem(model='auth.group')
        ]),
        ParentItem('Ingreso', children=[
            ChildItem('Ingreso', url='/admin/Prestamo/acceso/'),
            ChildItem(model='projects.acceso')

        ]),
        ParentItem('Reportes', children=[
            ChildItem('Reportes', url='/admin/Prestamo/acceso/')
        ]),
    )