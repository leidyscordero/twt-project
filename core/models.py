from django.db import models
from django.utils import timezone


# Create your models here.

# tipos de documento
class TipoDocumento(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    iniciales = models.CharField(max_length=3, null=False)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Tipo De Documento"
        verbose_name_plural = "Tipos de Documentos"
        default_permissions = ()
        permissions = (
            ("add_type_document", "Puede guardar tipos de documento"),
            ("view_type_document", "Puede ver tipos de documento"),
            ("change_type_document", "Puede actualizar tipos de documento"),
            ("delete_type_document", "Puede eliminar tipos de documento"),
        )

    def __str__(self):
        return str(self.iniciales)


# arl
class Arl(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    direccion = models.CharField(max_length=30, null=True, blank=True)
    telefono = models.CharField(max_length=30, null=True, blank=True)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Aseguradora (ARL)"
        verbose_name_plural = "Aseguradoras (Arl)"
        default_permissions = ()
        permissions = (
            ("add_arl", "Puede guardar aseguradoras (arl)"),
            ("view_arl", "Puede ver aseguradoras (arl)"),
            ("change_arl", "Puede actualizar aseguradoras (arl)"),
            ("delete_arl", "Puede eliminar aseguradoras (arl)"),
        )

    def __str__(self):
        return str(self.nombre)

#eps
class Eps(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    direccion = models.CharField(max_length=30, null=True, blank=True)
    telefono = models.CharField(max_length=30, null=True, blank=True)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Eps"
        verbose_name_plural = "Eps"
        default_permissions = ()
        permissions = (
            ("add_eps", "Puede guardar eps"),
            ("view_eps", "Puede ver eps"),
            ("change_eps", "Puede actualizar eps"),
            ("delete_eps", "Puede eliminar eps"),
        )

    def __str__(self):
        return str(self.nombre)

class Fase(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Fase"
        verbose_name_plural = "Fases"
        default_permissions = ()
        permissions = (
            ("add_fase", "Puede guardar fase"),
            ("view_fase", "Puede ver fase"),
            ("change_fase", "Puede actualizar fase"),
            ("delete_fase", "Puede eliminar fase"),
        )

    def __str__(self):
        return str(self.nombre)


class Profesion(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    estado = models.BooleanField(null=False, default=True)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    fecha_actualizacion = models.DateTimeField(null=True, blank=True)
    fecha_eliminacion = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "Profesión"
        verbose_name_plural = "Profesiones"
        default_permissions = ()
        permissions = (
            ("add_profesion", "Puede guardar profesiones"),
            ("view_profesion", "Puede ver profesiones"),
            ("change_profesion", "Puede actualizar profesiones"),
            ("delete_profesion", "Puede eliminar profesiones"),
        )

    def __str__(self):
        return str(self.nombre)
