from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib import admin

# Create your views here.


def inicio(request):
    return render(request, 'core/inicio.html')

@login_required(redirect_field_name='core/prestamo.html')
def prestamo_prestar(request):
    context = admin.site.each_context(request)
    context.update({
        'title': 'Prestar',
    })
    return render(request, 'core/prestamo.html', context)