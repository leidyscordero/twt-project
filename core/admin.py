from django.contrib import admin
from .models import *


# admin tipo documentos
class TipoDocumentos(admin.ModelAdmin):
    list_filter = ('estado',)
    search_fields = ('name', 'initials',)
    list_display = ['id', 'nombre', 'iniciales', 'estado']

    class Meta:
        model = TipoDocumento

# # admin arl
class Aseguradoras(admin.ModelAdmin):
    list_filter = ('estado',)
    search_fields = ('name', 'id',)
    list_display = ['id', 'nombre', 'direccion','telefono', 'estado']

    class Meta:
        model = Arl
        
# # admin eps
class Epss(admin.ModelAdmin):
    list_filter = ('estado',)
    search_fields = ('name', 'id',)
    list_display = ['id', 'nombre', 'direccion','telefono', 'estado']
    class Meta:
        model = Eps

class Fases(admin.ModelAdmin):
    list_filter = ('estado',)
    search_fields = ('name', 'id',)
    list_display = ['id', 'nombre', 'estado']
    class Meta:
        model = Fase

class Profesiones(admin.ModelAdmin):
    list_filter = ('estado',)
    search_fields = ('name', 'id',)
    list_display = ['id', 'nombre', 'estado']
    class Meta:
        model = Profesion


admin.site.register(TipoDocumento, TipoDocumentos)
admin.site.register(Arl, Aseguradoras)
admin.site.register(Eps, Epss)
admin.site.register(Fase, Fases)
admin.site.register(Profesion, Profesiones)