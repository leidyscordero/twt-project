from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
from . import views
from django.urls import path, re_path

urlpatterns = [
    re_path(r'^trabajador/(?P<numero_tarjeta>\d+)$', views.TrabajadorDetail.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)