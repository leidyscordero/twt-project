# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from django_filters.rest_framework import DjangoFilterBackend
import django_filters.rest_framework as filters
from datetime import datetime
from peoples.models import Trabajador
from projects.models import Acceso, Liquidacion, Proyecto
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from django.utils import timezone


class TrabajadorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trabajador
        fields = ('id','profesion','tipo_documento','numero_tarjeta','nombre','apellidos')


class TrabajadorDetail(generics.RetrieveUpdateAPIView):
    queryset = Trabajador.objects.filter(fecha_eliminacion=None)
    serializer_class = TrabajadorSerializer
    http_method_names = ['get']
    permission_classes = (AllowAny,)

    def retrieve(self, request, numero_tarjeta):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = TrabajadorSerializer
        try:
            obj = get_object_or_404(queryset, numero_tarjeta=numero_tarjeta)
            acceso = Acceso.objects.filter(trabajador=obj.id)
            if len(acceso) == 0:
                liquidacion =  Liquidacion.objects.filter(trabajador=obj.id,estado=True)
                if len(liquidacion) != 0:
                    print(liquidacion[0].proyecto.id)
                    Acceso.objects.create(proyecto=liquidacion[0].proyecto, trabajador=obj)
                    data = {'message': 'Ingreso Correcto', 'code': 2, 'data': True}
                    return Response(data, status=200)
                else:
                    data = {'message': 'No Tiene Contrato Activo', 'code': 2, 'data': None}
                    return Response(data, status=200)
            else:
                bandera = False
                for key in acceso:
                    if key.hora_salida is None:
                        # key.update(hora_salida=timezone.now)
                        Acceso.objects.filter(pk=key.id).update(hora_salida=timezone.now())
                        bandera = True
                        data = {'message': 'Salida Correcta', 'code': 2, 'data': True}
                        return Response(data, status=200)
                        break
                    
                if bandera == False:
                    liquidacion =  Liquidacion.objects.filter(trabajador=obj.id,estado=True)
                    if len(liquidacion) != 0:
                        print(liquidacion[0].proyecto.id)
                        Acceso.objects.create(proyecto=liquidacion[0].proyecto, trabajador=obj)
                        data = {'message': 'Ingreso Correcto', 'code': 2, 'data': True}
                        return Response(data, status=200)
                    else:
                        data = {'message': 'No Tiene Contrato Activo', 'code': 2, 'data': None}
                        return Response(data, status=200)

        except:
            data = {'message': 'Tabajador no encontrado', 'code': 2, 'data': None}
            return Response(data, status=200)